package com.itb.data;

import com.itb.utilities.ByteFormat;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

/**
 * Class of data management and processes.
 *
 * @author Ricardo Montserrat Solorzano
 * @version 6.1
 */
public class InputData {

    /**
     * Pause method to give some space to the user.
     */
    public static void pause() {
        Scanner reader = new Scanner(System.in);
        System.out.print("\nPress enter to continue...");
        reader.nextLine();
    }

    /**
     * Petition of a string to the user.
     *
     * @param message the petition to write.
     * @return the answer of the user.
     */
    public static String askUserString(String message) {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);
        return reader.nextLine();
    }

    /**
     * Shows an array of files.
     *
     * @param arrayFiles the array of files to show.
     * @return a String of array of files.
     */
    public static String showArray(File[] arrayFiles) {
        StringBuilder toBeString = new StringBuilder();
        for (File f : arrayFiles) {
            if (f.isDirectory()) toBeString.append("[DIR] ");
            else toBeString.append("[FILE] ");
            toBeString.append(f);
            toBeString.append("\n");
        }
        return toBeString.toString();
    }

    /**
     * Shows an array of strings.
     *
     * @param arrayStrings the array of strings to show.
     * @return a single String of array of strings.
     */
    public static String showArray(String[] arrayStrings) {
        StringBuilder toBeString = new StringBuilder();
        for (String h : arrayStrings) {
            toBeString.append(h);
            toBeString.append("\n");
        }
        return toBeString.toString();
    }

    /**
     * Shows all the parents of a directory.
     *
     * @param userDir the directory to work with.
     * @return a string with the parents.
     */
    public static String showParents(File userDir) {
        ByteFormat byteFormat = new ByteFormat();
        if (userDir.getParent() != null)
            return userDir.toString() + " Free Space is " + byteFormat.formatKB(userDir.getFreeSpace()) + "\n" + showParents(userDir.getParentFile());
        return userDir.toString() + " Free Space is " + byteFormat.formatKB(userDir.getFreeSpace());
    }

    /**
     * Shows the directories, files and sub files of subdirectories
     *
     * @param userDir the directory to work with.
     */
    public static void showDirectorySubFiles(File userDir) {
        ByteFormat byteFormat = new ByteFormat();
        File[] arrayFiles = userDir.listFiles();
        System.out.println("[MAIN DIR] " + userDir.getName());
        assert arrayFiles != null;
        for (File f : arrayFiles) {
            Date fDate = new Date(f.lastModified());
            if (f.isDirectory()) {
                System.out.println("[SUB DIR] of " + userDir.getName() + ": " + f.getName() + ", free space of " + byteFormat.formatKB(f.getFreeSpace()) + " KiloBytes, last modification in " + fDate);
                showDirectorySubFiles(f);
            } else
                System.out.println("[SUB FILE] of " + userDir.getName() + ": " + f.getName() + ", size of " + byteFormat.formatKB(f.length()) + ", last modification in " + fDate);
        }
    }

    /**
     * Searches a directory
     *
     * @param nameDir   the name to look for.
     * @param startPath the path to start looking in.
     * @return true if founded.
     */
    public static boolean searchDirectory(String nameDir, File startPath) {
        File[] userPath = startPath.listFiles();
        assert userPath != null;
        for (File f : userPath) {
            if (f.getName().equalsIgnoreCase(nameDir) && f.isDirectory()) {
                System.out.println(f);
                return true;
            }
        }
        return false;
    }

    /**
     * Searches for a file.
     *
     * @param fileName     name of the file to look for.
     * @param startPath    the path to start looking in.
     * @param showHidden   if you want to see hidden files or don't.
     * @param typeOfSearch you can search for full name, part of the name or extension.
     * @return true if founded.
     */
    public static boolean searchFile(String fileName, File startPath, boolean showHidden, String typeOfSearch) {
        File[] userPath = startPath.listFiles();
        boolean found = false;
        switch (typeOfSearch) {
            case "name":
                assert userPath != null;
                for (File f : userPath) {
                    if (f.isFile() && f.getName().equalsIgnoreCase(fileName)) {
                        if (f.isHidden() && showHidden || !f.isHidden()) {
                            System.out.println(f);
                            found = true;
                        }
                    }
                }
                break;
            case "part of name":
                assert userPath != null;
                for (File f : userPath) {
                    if (f.isFile() && (f.isHidden() && showHidden || !f.isHidden()) && f.getName().contains(fileName)) {
                        System.out.println(f);
                        found=true;
                    }
                }
                break;
            case "extension name":
                int sequenceLength = fileName.length();
                assert userPath != null;
                for (File f : userPath) {
                    if (f.isFile()) {
                        boolean sameExtension = true;
                        int counter = 1;
                        for (int i = f.getName().length() - 1; i > f.getName().length() - sequenceLength; i--) {
                            if (f.getName().charAt(i) != fileName.charAt(fileName.length() - counter))
                                sameExtension = false;
                            counter++;
                        }
                        if (sameExtension && (f.isHidden() && showHidden || !f.isHidden())) {
                            System.out.println(f);
                            found = true;
                        }
                    }
                }
                break;
        }
        return found;
    }
}