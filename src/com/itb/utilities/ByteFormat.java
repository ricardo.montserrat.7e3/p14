package com.itb.utilities;
import java.text.*;
/**
 * Utility class to format the bytes of files information.
 */
public class ByteFormat extends Format {
    
    public ByteFormat(){}

    public String formatKB(long kilobytes){
        return format(kilobytes * 1024);
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer buf, FieldPosition pos) {
        if (obj instanceof Long) {
            long numBytes = (Long) obj;
            if (numBytes < 1024*1024) {  
                DecimalFormat formatter = new DecimalFormat("#,##0.0");
                buf.append(formatter.format((double)numBytes/1024.0)).append(" KB");  
            } else if(numBytes < 1024*1024*1024) {  
                DecimalFormat formatter = new DecimalFormat("#,##0.0");  
                buf.append(formatter.format((double)numBytes/(1024.0*1024.0))).append(" MB");  
            } else {
                DecimalFormat formatter = new DecimalFormat("#,##0.0");  
                buf.append(formatter.format((double)numBytes/(1024.0*1024.0*1024.0))).append(" GB");  
            } 
        }  
        return buf;  
    }

    @Override
    public Object parseObject(String string, ParsePosition pp) {
        return null;
    }

}