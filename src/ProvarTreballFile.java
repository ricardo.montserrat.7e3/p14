import com.itb.data.InputData;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Main class to start the project.
 *
 * @author Ricardo Montserrat Solorzano
 * @version 6.1
 */
public class ProvarTreballFile {
    public static void main(String[] args) throws IOException {
        ProvarTreballFile program = new ProvarTreballFile();
        program.start();
    }

    /**
     * Basic structure of the program.
     *
     * @throws IOException in case of a weird exception.
     */
    private void start() throws IOException {
        boolean trialFinished;
        do {
            showMenu();
            trialFinished = menuOptionSelection();
        } while (!trialFinished);
    }

    /**
     * Shows the main menu.
     */
    private void showMenu() {
        System.out.print("WELCOME TO THE MENU!!!" +
                "\n\n" +
                "1.- File or directory and its full path." +
                "\n" +
                "2.- Directory files and sub directories." +
                "\n" +
                "3.- Create, rename, delete a file or directory." +
                "\n" +
                "4.- Parents of a directory." +
                "\n" +
                "5.- Sub directories and sub files full information." +
                "\n" +
                "6.- Files explorer." +
                "\n" +
                "7.- Exit." +
                "\n\n"
        );
    }

    /**
     * Let's the user select an option.
     *
     * @return boolean if the program is finished.
     * @throws IOException in case of weird exception.
     */
    private boolean menuOptionSelection() throws IOException {
        switch (InputData.askUserString("I want to try...").toLowerCase()) {
            case "1":
            case "full paths":
            case "full path":
            case "file path":
            case "directory path":
                directoryPath();
                break;
            case "2":
            case "directory":
            case "directory files":
            case "directory sub directories":
                directoryFiles();
                break;
            case "3":
            case "create":
            case "create file":
            case "create directory":
            case "rename":
            case "rename file":
            case "rename directory":
            case "delete":
            case "delete file":
            case "delete directory":
                createRenameDeleteFileDirectory();
                break;
            case "4":
            case "parents":
            case "parents of directory":
            case "parents of a directory":
            case "parents of directories":
                parentsOfDirectory();
                break;
            case "5":
            case "sub":
            case "sub directories":
            case "sub files":
            case "full information":
                subFilesSubDirectories();
                break;
            case "6":
            case "file explorer":
            case "explorer":
                theExplorer();
                break;
            case "7":
            case "e":
            case "exit":
                System.out.println("GOODBYE!!! :D");
                return true;
            default:
                System.out.println("That's not an option, please Try AGAIN!!!\n");
        }
        InputData.pause();
        return false;
    }

    /**
     * Selection of option 1 directoryPath
     */
    private void directoryPath() {
        File userFile = new File(InputData.askUserString("Please,  insert the path for the file/directory: "));
        if (userFile.exists()) {
            System.out.print("The written path exist ");
            if (userFile.isFile()) System.out.println("and is a \"FILE\".");
            else System.out.println("and is a \"DIRECTORY\"");
            System.out.println("The absolute path is: " + userFile.getAbsolutePath());
            System.out.println("The relative path is: " + userFile.getPath());
        } else System.out.println("The written path does not exist.\n");
    }

    /**
     * Selection of option 2.
     */
    private void directoryFiles() {
        File userFile = new File(InputData.askUserString("Please,  insert the path for a directory: "));
        if (userFile.exists()) {
            System.out.println(InputData.showArray(Objects.requireNonNull(userFile.listFiles())));
            System.out.println(InputData.showArray(Objects.requireNonNull(userFile.list())));
        } else System.out.println("The written path does not exist.\n");
    }

    /**
     * Selection of option 3 create,Rename,Delete,File/Directory
     */
    private void createRenameDeleteFileDirectory() throws IOException {
        File userFile = new File(InputData.askUserString("Please,  insert the path of the directory to work with: "));
        if (userFile.exists()) {
            switch (InputData.askUserString("Select an option:\n\n[1]Rename File/Directory.\n[2]Delete File/Directory.\n\nI want to...").toLowerCase()) {
                case "1":
                case "rename":
                    renameDirectoryFile(userFile);
                    break;
                case "2":
                case "delete":
                    deleteDirectoryFile(userFile);
                    break;
                default:
                    System.out.println("That's not an option.\n");
            }
        } else {
            switch (InputData.askUserString("The path written does not exist.\nDo you wish to create it?\nyes/no: ").toLowerCase()) {
                case "yes":
                case "ye":
                case "y":
                    createDirectoryFile(userFile);
                    break;
                case "no":
                case "n":
                    break;
                default:
                    System.out.println("That's not an option.");
            }
        }
    }

    /**
     * Method to delete a directory.
     *
     * @param userFile path of the user.
     */
    private void deleteDirectoryFile(File userFile) {
        if (userFile.delete()) System.out.println("The path was deleted successfully\n");
        else System.out.println("There was a problem while deleting the path.\n");
    }

    /**
     * Method to rename a directory.
     *
     * @param userFile path of the user.
     */
    private void renameDirectoryFile(File userFile) {
        if (userFile.renameTo(new File(userFile.getParentFile().toString() + File.separator + InputData.askUserString("Please,  insert the new name for the directory/file: "))))
            System.out.println("The process is finished and it renamed.");
        else System.out.println("There was a problem while renaming the path.");
    }

    /**
     * Method to create a directory.
     *
     * @param userFile path of the user.
     * @throws IOException in case of a weird exception.
     */
    private void createDirectoryFile(File userFile) throws IOException {
        switch (InputData.askUserString("Do you want to create a:\n\n[1]Directory\n[2]File\n\nI want to create a...").toLowerCase()) {
            case "1":
            case "directory":
            case "d":
                if (userFile.mkdir()) System.out.println("The process is finished and the directory was created.");
                else System.out.println("There was a problem while creating the directory.\n");
                break;
            case "2":
            case "file":
            case "f":
                if (userFile.createNewFile()) System.out.println("The process is finished and the file was created.");
                else System.out.println("There was a problem while creating the file.\n");
                break;
            default:
                System.out.println("That's not an option.\n");
        }
    }

    /**
     * Selection of option 4 parents Of Directory
     */
    private void parentsOfDirectory() {
        File userFile = new File(InputData.askUserString("Please, insert the directory path, to see its parents: "));
        if (userFile.exists() && userFile.isDirectory()) System.out.println(InputData.showParents(userFile));
        else System.out.println("The path doesn't exist or it isn't the path of a directory.");
    }

    /**
     * Selection of option 5 subFiles and SubDirectories
     */
    private void subFilesSubDirectories() {
        File userFiles = new File(InputData.askUserString("Please, insert the directory path, to see its files and subdirectories: "));
        InputData.showDirectorySubFiles(userFiles);
    }

    /**
     * Selection of option 6 theExplorer
     */
    private void theExplorer() {
        File startPath;
        do {
            startPath = new File(InputData.askUserString("Please, insert the directory path to start looking from: "));
            if (!startPath.isDirectory()) System.out.println("The path is not a directory path.");
        } while (!startPath.isDirectory());
        switch (InputData.askUserString("Welcome to the EXPLORER!!!\n\n[1]Look for Directory\n[2]Look for File\n\nI want to...").toLowerCase()) {
            case "1":
            case "directory":
                if (InputData.searchDirectory(InputData.askUserString("Please, insert the directory name: "), startPath))
                    System.out.println("The directory was succesfully found!");
                else System.out.println("The directory was not found.");
                break;
            case "2":
            case "file":
                boolean typeSelected, showHidden = false;
                do {
                    typeSelected = true;
                    switch (InputData.askUserString("Do you want to look in hidden files and directories? yes/no: ")) {
                        case "yes":
                        case "y":
                            showHidden = true;
                            break;
                        case "no":
                        case "n":
                            showHidden = false;
                            break;
                        default:
                            typeSelected = false;
                    }
                } while (!typeSelected);
                String typeOfSearch = "";
                do {
                    typeSelected = true;
                    switch (InputData.askUserString("What type of search are you looking for?\n\n[1]Name\n[2]Part of Name\n[3]Extension Name.\n\nI want to search for...").toLowerCase()) {
                        case "1":
                        case "name":
                            typeOfSearch = "name";
                            break;
                        case "2":
                        case "part of name":
                        case "part name":
                        case "part":
                            typeOfSearch = "part of name";
                            break;
                        case "3":
                        case "ext":
                        case "extension name":
                        case "extension":
                            typeOfSearch = "extension name";
                            break;
                        default:
                            System.out.println("That's not an option...");
                            typeSelected = false;
                    }
                } while (!typeSelected);
                if (InputData.searchFile(InputData.askUserString("Please, insert file name to look for: "), startPath, showHidden, typeOfSearch))
                    System.out.println("The files were found successfully!");
                else System.out.println("The files were not found");
                break;
            default:
                System.out.println("That's not an option.");
        }
    }
}
